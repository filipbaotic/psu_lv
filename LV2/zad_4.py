import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
img_new = img.copy()

rows, cols = img.shape
for i in range(rows):
    for j in range(cols):
        if img[i][j] > 200:
            img_new[i][j] = 255
        else:
            img_new[i][j] += 55


rotated = np.zeros((cols,rows))
for i in range(rows):
    for j in range(cols):
        rotated[j][rows-i-1] = img[i][j]

mirror = img.copy()
for i in range(rows):
    for j in range(cols):
        mirror[i][j] = img[i][cols-1-j]

sr=int(rows/10)
sc=int(cols/10)
small = np.zeros((sr,sc))
for i in range(sr):
    for j in range(sc):
        small[i][j] = img[10*i][10*j]


show = np.zeros((rows,cols))
x = int(cols/4)
for i in range(rows):
    for j in range(x, 2*x):
        show[i][j] = img[i][j]

#plt.figure(1)
#plt.imshow(img, cmap='gray', vmin=0, vmax=255)
#plt.figure(2)
#plt.imshow(img_new, cmap='gray', vmin=0, vmax=255)
#plt.figure(3)
#plt.imshow(rotated, cmap='gray', vmin=0, vmax=255)
#plt.figure(4)
#plt.imshow(mirror, cmap='gray', vmin=0, vmax=255)
#plt.figure(5)
#plt.imshow(small, cmap='gray', vmin=0, vmax=255)
plt.figure(6)
plt.imshow(show, cmap='gray', vmin=0, vmax=255)

plt.show()
