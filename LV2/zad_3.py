import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(open("LV2\mtcars.csv", "rb"), usecols=(1,2,3,4,5,6),
delimiter=",", skiprows=1)

mpg = data[:,0]
hp = data[:,3]
wt = data[:,5]

print("Min MPG: ", np.min(mpg))
print("Max MPG: ", np.max(mpg))
print("Mean MPG: ", np.mean(mpg))
plt.scatter(mpg,hp, s=wt*15)
plt.xlabel('Potrošnja automobila')
plt.ylabel('Konjska snaga')
plt.title('Ovisnost potrošnje i snage')
plt.show()
