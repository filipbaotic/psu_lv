import numpy as np
import matplotlib.pyplot as plt

bacanje = np.empty(0, int)


for x in range(100):
    bacanje = np.append(bacanje, np.random.randint(1,7))


k1 = np.count_nonzero(bacanje == 1)
k2 = np.count_nonzero(bacanje == 2)
k3 = np.count_nonzero(bacanje == 3)
k4 = np.count_nonzero(bacanje == 4)
k5 = np.count_nonzero(bacanje == 5)
k6 = np.count_nonzero(bacanje == 6)
kolicina = [k1, k2, k3, k4, k5, k6]


print(bacanje)

plt.bar(bacanje, kolicina)
plt.show()