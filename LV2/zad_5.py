import matplotlib.pyplot as plt
import numpy as np

# s - size of square
# r - rows
# c - columns
# x - photo

def funkc(s, r, c):
    w = 255 * np.ones((s,s))
    b = np.zeros((s,s))
    sr = s*r
    sc = s*c
    x = [sr][sc]
    
    r1=b
    r2=w

    for i in range(sr-1):
        if i%2==0:
            r1 = np.hstack((r1,w))
            r2 = np.hstack((r2,b))
        else:
            r1 = np.hstack((r1,b))
            r2 = np.hstack((r2,w))
        
    return x

slika = funkc(50, 5, 5)

plt.imshow(slika, cmap='gray', vmin=0, vmax=255)
plt.show()
