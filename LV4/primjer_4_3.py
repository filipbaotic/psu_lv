import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# ucitavanje ociscenih podataka
df = pd.read_csv('LV4/cars_processed.csv')
print(df.info())

# razliciti prikazi
sns.pairplot(df, hue='fuel')

sns.relplot(data=df, x='km_driven', y='selling_price', hue='fuel')
df = df.drop(['name','mileage'], axis=1)

obj_cols = df.select_dtypes(object).columns.values.tolist()
num_cols = df.select_dtypes(np.number).columns.values.tolist()

fig = plt.figure(figsize=[15,8])
for col in range(len(obj_cols)):
    plt.subplot(2,2,col+1)
    sns.countplot(x=obj_cols[col], data=df)

df.boxplot(by ='fuel', column =['selling_price'], grid = False)

df.hist(['selling_price'], grid = False)

tabcorr = df.corr()
sns.heatmap(df.corr(), annot=True, linewidths=2, cmap= 'coolwarm') 

# plt.show()

# 1. broj mjerenja: 6699
# 2. ispisano je pod Dtype
# 3. najveca cijena: 15,78 BMW x7 2020
# print(df.sort_values(by = 'selling_price').head(1))
#    najmanja cijena: 10,30 Maruti 1997
# print(df.sort_values(by = 'selling_price').tail(1))
# 4. 575
# print(sum(df['year']==2012))
# 5. 
# print(df.sort_values(by = 'km_driven').head(1))
# print(df.sort_values(by = 'km_driven').tail(1))
# 6. 
# 7. 
# 8. 