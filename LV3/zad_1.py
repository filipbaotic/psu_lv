import pandas as pd

mtcars = pd.read_csv('LV3/mtcars.csv')
cyl6=mtcars[mtcars.cyl==6]
cyl8=mtcars[mtcars.cyl==8]

a = mtcars.sort_values(by = 'mpg').head(5).car
b = cyl8.sort_values(by = 'mpg', ascending=False).head(3).car
c = cyl6.mean().mpg
d = mtcars[(mtcars.cyl==4) & (mtcars.wt>2) & (mtcars.wt<2.2)].mean().mpg
e_a = len(mtcars[mtcars.am==0])
e_m = len(mtcars[mtcars.am==1])
f = len(mtcars[(mtcars.am==1) & (mtcars.hp>100)])
g = mtcars['kg'] = mtcars['wt']*1000/2.205

print(mtcars[['car','kg']])
