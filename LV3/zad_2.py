#import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

mtcars = pd.read_csv('LV3/mtcars.csv')

#mtcars.groupby('cyl')['mpg'].mean().plot.bar()
#mtcars.boxplot(by='cyl', column='wt')
#mtcars.boxplot(by='am', column='mpg')
mtcars[mtcars.am==0].plot.scatter(x='qsec', y='hp', color="Blue", label="Automatic")
mtcars[mtcars.am==1].plot.scatter(x='qsec', y='hp', color="Red", label="Manual")


plt.show()
